import {Component} from '@angular/core';
import {NavController, AlertController, LoadingController} from 'ionic-angular';
import {Http, Headers, RequestOptions} from "@angular/http";
import 'rxjs/add/operator/map';
import { Storage, LocalStorage } from 'ionic-angular';


@Component({
  templateUrl: 'build/pages/home/home.html'
})

export class HomePage {
    data: any;
  constructor(private navCtrl: NavController, private http: Http, private alert: AlertController, private loading : LoadingController) {
      this.data = {};
      this.data.username = "";
      this.data.password = "";
  }

  login(){
      let username = this.data.username;
      let password = this.data.password;
      let data = JSON.stringify({username, password});
      let link = "http://localhost:8000/loginUser";
      let headers = new Headers({ 'Content-Type': 'application/json' });
      let options = new RequestOptions({headers: headers});

      let args = [1];


      this.http.post( link, data, options).subscribe(data => {
          let loader = this.loading.create({
                 content: "Checking ! Please wait...",
                 duration: 1000
          });
          loader.present();
          alert("Success");
      }, error => {
          let alert = this.alert.create({
                title: 'Warning',
                subTitle: 'Wrong Username or Password! Please Try Again !',
                buttons: ['OK']
            });
            alert.present();
      });
  }


}
